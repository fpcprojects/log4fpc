{$ifndef ALLPACKAGES}

{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;

{$endif ALLPACKAGES}

procedure add_log4fpc(const ADirectory: string);

var
  P : TPackage;
  T : TTarget;
  D : TDependency;

begin
  with Installer do
    begin
    P:=AddPackage('log4fpc');
    P.Version:='0.9.0-1';
    P.Description:='Library for easy logging';
    P.License:='Apache License Version 2';
    P.Author:='Log4Delphi/Joost van der Sluis';
    P.Email:='joost@cnoc.nl';
    P.HomepageURL:='https://gitlab.com/fpcprojects/log4fpc';

    P.Directory:=ADirectory;

    P.Options.Add('-MObjFPC');
    P.Options.Add('-Scgi');
    P.Options.Add('-O1');
    P.Options.Add('-g');
    P.Options.Add('-gl');
    P.Options.Add('-l');
    P.Options.Add('-vewnhibq');

    P.Sourcepath.Add('src');
    P.Sourcepath.Add('src/util');

    P.Dependencies.Add('fcl-base');

    T := P.Targets.AddUnit('TAppenderUnit.pas');

    T := P.Targets.AddUnit('TFileAppenderUnit.pas');
    D := T.Dependencies.AddUnit('TPrintWriterUnit');

    T := P.Targets.AddUnit('TWriterAppenderUnit.pas');
    T := P.Targets.AddUnit('TNullAppenderUnit.pas');
    T := P.Targets.AddUnit('TPrintWriterUnit.pas');
    T := P.Targets.AddUnit('TRollingFileAppenderUnit.pas');
    T := P.Targets.AddUnit('THTMLLayoutUnit.pas');
    T := P.Targets.AddUnit('TXMLLayoutUnit.pas');
    T := P.Targets.AddUnit('TSimpleLayoutUnit.pas');
    T := P.Targets.AddUnit('TStringUnit.pas');
    T := P.Targets.AddUnit('TLayoutUnit.pas');
    T := P.Targets.AddUnit('TOnlyOnceErrorHandlerUnit.pas');
    T := P.Targets.AddUnit('teventlogappenderunit.pas');
    T := P.Targets.AddUnit('tconsoleappenderunit.pas');

    T := P.Targets.AddUnit('TPatternLayoutUnit.pas');
    D := T.Dependencies.AddUnit('TStringUnit');

    T := P.Targets.AddUnit('TLoggerUnit.pas');
    T := P.Targets.AddImplicitUnit('TLoggingEventUnit.pas');
    T := P.Targets.AddImplicitUnit('TLogLogUnit.pas');
    T := P.Targets.AddImplicitUnit('TErrorHandlerUnit.pas');
    T := P.Targets.AddImplicitUnit('TLayoutUnit.pas');
    T := P.Targets.AddImplicitUnit('TLevelUnit.pas');

    P.Sources.AddDocFiles('documentation/*', '', True, 'documentation');
    P.Sources.AddDoc('NOTICE.txt');
    P.Sources.AddDoc('README.txt');
    P.Sources.AddDoc('LICENSE.txt');
    end;
end;

{$ifndef ALLPACKAGES}
begin
  add_log4fpc('');
  Installer.Run;
end.
{$endif ALLPACKAGES}
