{
   Copyright 2019 Joost van der Sluis / CNOC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
}
{*----------------------------------------------------------------------------
   Contains the TConsoleAppender class.
   @version 0.1
   @author <a href="mailto:joost@cnoc.nl">Joost van der Sluis</a>
  ----------------------------------------------------------------------------}
unit TConsoleAppenderUnit;

{$ifdef fpc}
  {$mode objfpc}
  {$h+}
{$endif}

interface

uses
   Classes,
   sysutils,
   TAppenderUnit,
   TLayoutUnit,
   TSimpleLayoutUnit,
   TLoggingEventUnit,
   TLevelUnit,
   TPrintWriterUnit;

type
{*----------------------------------------------------------------------------
   TConsoleAppender writes log events to the console.
  ----------------------------------------------------------------------------}

   { TConsoleAppender }

   TConsoleAppender = class (TAppender)
   private
      FOwnsLayout: Boolean;
   protected
      procedure WriteFooter();
      procedure WriteHeader();
   public
      constructor Create(ALayout : TLayout);
      constructor Create();
      destructor Destroy; Override;
      procedure Close();
      procedure Append(AEvent : TLoggingEvent); Override;
      function RequiresLayout() : Boolean; Override;
   end;

implementation

uses
   TLogLogUnit;

{*----------------------------------------------------------------------------
   Write a footer as produced by the embedded layout's Layout.getFooter
   method.
  ----------------------------------------------------------------------------}
procedure TConsoleAppender.WriteFooter();
var
  s: string;
begin
  if (not Self.FClosed) then
    begin
    s := Self.FLayout.GetFooter;
    if s <> '' then
      WriteLn(s);
    end;
end;

{*----------------------------------------------------------------------------
   Write a header as produced by the embedded layout's Layout.getHeader method.
  ----------------------------------------------------------------------------}
procedure TConsoleAppender.WriteHeader();
var
  s: string;
begin
  if (not Self.FClosed) then
    begin
    s := Self.FLayout.GetHeader;
    if s <> '' then
      WriteLn(etInfo, s);
    end;
end;

{*----------------------------------------------------------------------------
   Instantiate a EventLogAppender.
   @param ALayout The layout to use
  ----------------------------------------------------------------------------}
constructor TConsoleAppender.Create(ALayout: TLayout);
begin
  inherited Create;
  FLayout := ALayout;
  WriteHeader;
end;

{*----------------------------------------------------------------------------
   Instantiate a EventLogAppender using a TSimpleLayout.
  ----------------------------------------------------------------------------}
constructor TConsoleAppender.Create();
begin
  Create(TSimpleLayout.Create);
  FOwnsLayout := True;
end;


{*----------------------------------------------------------------------------
   Destruct this instance by freeing the contained TEventLog instance.
  ----------------------------------------------------------------------------}
destructor TConsoleAppender.Destroy;
begin
   if not Self.FClosed then
      Self.Close;
   if FOwnsLayout then
     TLogLog.debug('TWriterAppender#Destroy');
   inherited Destroy;
end;

{*----------------------------------------------------------------------------
   Close this appender instance. Closed appenders cannot be reused.
  ----------------------------------------------------------------------------}
procedure TConsoleAppender.Close();
begin
   if (Self.FClosed) then
      exit;
   if (self.FLayout <> Nil) then
      Self.WriteFooter;
  Self.FClosed := true;
end;

{*----------------------------------------------------------------------------
   If the writer exists and is writable then write a log statement to the
   system-log.
   @param AEvent The event to log
  ----------------------------------------------------------------------------}
procedure TConsoleAppender.Append(AEvent : TLoggingEvent);
begin
   if (Self.FClosed) then begin
      if (Self.FErrorHandler <> Nil) then
         Self.FErrorHandler.Error(
            'This appender is closed and cannot be written to.');
      Exit;
   end;

   WriteLn(Self.Flayout.Format(AEvent));
   if not (Self.Flayout.IgnoresException) then
      if (AEvent.getException <> Nil) then
         Writeln('Exception: ' + AEvent.GetException.Message);
end;

{*----------------------------------------------------------------------------
   Determines if this appender requires a layout. This appender does require
   a layout.
   @return True since this appender requires a layout
  ----------------------------------------------------------------------------}
function TConsoleAppender.RequiresLayout() : Boolean;
begin
   Result := true;
end;


end.
