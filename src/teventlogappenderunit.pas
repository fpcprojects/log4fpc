{
   Copyright 2017 Joost van der Sluis / CNOC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
}
{*----------------------------------------------------------------------------
   Contains the TEventLogAppender class.
   @version 0.1
   @author <a href="mailto:joost@cnoc.nl">Joost van der Sluis</a>
  ----------------------------------------------------------------------------}
unit TEventLogAppenderUnit;

{$ifdef fpc}
  {$mode objfpc}
  {$h+}
{$endif}

interface

uses
   Classes,
   sysutils,
   eventlog,
   TAppenderUnit, TLayoutUnit, TLoggingEventUnit,
   TSimpleLayoutUnit,
   TLevelUnit, TPrintWriterUnit;

type
{*----------------------------------------------------------------------------
   TEventLogAppender appends log events to the system-log.
  ----------------------------------------------------------------------------}

   { TEventLogAppender }

   TEventLogAppender = class (TAppender)
   private
   protected
      FEventLog: TEventLog;
      procedure WriteFooter();
      procedure WriteHeader();
   public
      constructor Create(ALayout : TLayout = nil);
      destructor Destroy; Override;
      procedure Close();
      procedure Append(AEvent : TLoggingEvent); Override;
      function RequiresLayout() : Boolean; Override;
   end;

implementation

uses
   TLogLogUnit;

{*----------------------------------------------------------------------------
   Write a footer as produced by the embedded layout's Layout.getFooter
   method.
  ----------------------------------------------------------------------------}
procedure TEventLogAppender.WriteFooter();
var
  s: string;
begin
  if (not Self.FClosed) then
    begin
    s := Self.FLayout.GetFooter;
    if s <> '' then
      Self.FEventLog.Log(etInfo, s);
    end;
end;

{*----------------------------------------------------------------------------
   Write a header as produced by the embedded layout's Layout.getHeader method.
  ----------------------------------------------------------------------------}
procedure TEventLogAppender.WriteHeader();
var
  s: string;
begin
  if (not Self.FClosed) then
    begin
    s := Self.FLayout.GetHeader;
    if s <> '' then
      Self.FEventLog.Log(etInfo, s);
    end;
end;

{*----------------------------------------------------------------------------
   Instantiate a EventLogAppender.
   @param ALayout The layout to use
  ----------------------------------------------------------------------------}
constructor TEventLogAppender.Create(ALayout: TLayout);
begin
  inherited Create;
  FEventLog := TEventLog.Create(nil);
  FEventLog.LogType := ltSystem;
  if not Assigned(ALayout) then
    ALayout := TSimpleLayout.Create;
  FLayout := ALayout;
  WriteHeader;
end;

{*----------------------------------------------------------------------------
   Destruct this instance by freeing the contained TEventLog instance.
  ----------------------------------------------------------------------------}
destructor TEventLogAppender.Destroy;
begin
   if not Self.FClosed then
      Self.Close;
   FEventLog.Free;
   TLogLog.debug('TWriterAppender#Destroy');
   inherited Destroy;
end;

{*----------------------------------------------------------------------------
   Close this appender instance. Closed appenders cannot be reused.
  ----------------------------------------------------------------------------}
procedure TEventLogAppender.Close();
begin
   if (Self.FClosed) then
      exit;
   if (self.FLayout <> Nil) then
      Self.WriteFooter;
  Self.FClosed := true;
end;

{*----------------------------------------------------------------------------
   If the writer exists and is writable then write a log statement to the
   system-log.
   @param AEvent The event to log
  ----------------------------------------------------------------------------}
procedure TEventLogAppender.Append(AEvent : TLoggingEvent);
var
  EventType : TEventType;
begin
   if (Self.FClosed) then begin
      if (Self.FErrorHandler <> Nil) then
         Self.FErrorHandler.Error(
            'This appender is closed and cannot be written to.');
      Exit;
   end;
   Case AEvent.GetLevel.IntValue of
     FATAL_INT,
     ERROR_INT : EventType := etError;
     WARN_INT  : EventType := etWarning;
     INFO_INT  : EventType := etInfo;
     DEBUG_INT,
     TRACE_INT : EventType := etDebug;
   else
     EventType := etCustom;
   end;

   Self.FEventLog.Log(EventType, Self.Flayout.Format(AEvent));
   if not (Self.Flayout.IgnoresException) then
      if (AEvent.getException <> Nil) then
         Self.FEventLog.Log(etError, 'Exception: ' + AEvent.GetException.Message);
end;

{*----------------------------------------------------------------------------
   Determines if this appender requires a layout. This appender does require
   a layout.
   @return True since this appender requires a layout
  ----------------------------------------------------------------------------}
function TEventLogAppender.RequiresLayout() : Boolean;
begin
   Result := true;
end;

end.
